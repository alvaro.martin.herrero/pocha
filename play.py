####################################################################################################
# purpose: everything is set, let us play now
# date: 20190123
# notes:
####################################################################################################

####################################################################################################
# import modules
####################################################################################################
from bet import table
from shuffle import deck
from set_player_order import player_order

####################################################################################################
# functions
####################################################################################################


def winner_card(list_of_cards):
    """
    Determines which is the dominating card in a given trick
    :param list_of_cards: list with card names, e.g. '7_golds'
    :return: winner card name, suit and points
    """
    r_dominating_suit = deck[list_of_cards[0]].suit
    cards_to_compare = [x for x in list_of_cards if deck[x].suit == r_dominating_suit]
    r_dominating_card_points = max([deck[x].points for x in cards_to_compare])
    r_dominating_card = [x for x in cards_to_compare if deck[x].points == r_dominating_card_points][0]
    return r_dominating_card, r_dominating_suit, r_dominating_card_points


def cards_to_throw(list_of_cards, p_dominating_card, p_dominating_suit, p_dominating_card_points):
    """
    Determines the set of possible cards to throw for a given player
    :param list_of_cards: list with card names, e.g. '7_golds'
    :param p_dominating_card: dominating card name, e.g. 'ace_swords'
    :param p_dominating_suit: e.g. swords
    :param p_dominating_card_points: e.g. 10
    :return: list of possible cards to throw
    """
    if p_dominating_card == '':
        r_set_of_cards_to_throw = list_of_cards
    else:
        r_set_of_cards_to_throw = [x for x in list_of_cards if deck[x].suit == p_dominating_suit]
        r_set_of_cards_to_throw = [x for x in r_set_of_cards_to_throw if deck[x].points > p_dominating_card_points]
        if not r_set_of_cards_to_throw:
            r_set_of_cards_to_throw = [x for x in list_of_cards if deck[x].suit == p_dominating_suit]
            if not r_set_of_cards_to_throw:
                r_set_of_cards_to_throw = list_of_cards
    return r_set_of_cards_to_throw


####################################################################################################
# play
####################################################################################################
print('PLAYING NOW')
# initialise out of the loop variables
trick = 1
collected_deck = {}
tricks_to_play = len(table['player_me'].cards)
while trick <= tricks_to_play:
    print('--- trick', str(trick), '---')
    # initialise within loop variables
    dominating_card = ''
    dominating_suit = ''
    dominating_card_points = 0
    cards_in_trick = []
    for i in player_order:
        set_of_cards_to_throw = cards_to_throw(table[i].cards,
                                               dominating_card,
                                               dominating_suit,
                                               dominating_card_points)
        if i == 'player_me':
            print('your cards are\n', table[i].cards)
            print('which card do you want to throw? Choose one of these \n', set_of_cards_to_throw)
            while True:
                card_thrown = str(input('-->'))
                if card_thrown not in set_of_cards_to_throw:
                    print('please choose a valid card, from this list \n', set_of_cards_to_throw)
                else:
                    break
        else:
            best_card_points = max([deck[x].points for x in set_of_cards_to_throw])
            best_card = [x for x in set_of_cards_to_throw if deck[x].points == best_card_points][0]
            worst_card_points = min([deck[x].points for x in set_of_cards_to_throw])
            worst_card = [x for x in set_of_cards_to_throw if deck[x].points == worst_card_points][0]
            if i == player_order[0]:
                if table[i].asked > table[i].made:
                    if best_card_points == 10:
                        card_thrown = best_card
                    else:
                        card_thrown = worst_card
                else:
                    card_thrown = worst_card
            else:
                if table[i].asked > table[i].made and dominating_suit in [deck[x].suit for x in set_of_cards_to_throw]:
                    if best_card_points == 10:
                        card_thrown = best_card
                    else:
                        card_thrown = worst_card
                else:
                    card_thrown = worst_card
        # put the card on the table and compare it against the other cards in the trick
        print(i, 'plays', card_thrown)
        table[i].cards.remove(card_thrown)
        cards_in_trick.append(card_thrown)
        dominating_card, dominating_suit, dominating_card_points = winner_card(cards_in_trick)
    # collect cards
    collected_deck['trick_' + str(trick)] = cards_in_trick
    # determine who wins the trick and alter player_order to continue playing
    winner_player_index = cards_in_trick.index(dominating_card)
    winner_player = player_order[winner_player_index]
    player_order = player_order[winner_player_index:] + player_order[:winner_player_index]
    table[winner_player].made += 1
    print('---')
    print(winner_player, 'wins this trick\n')
    # next trick
    trick += 1
print('---+---+---\n')

####################################################################################################
# count points
####################################################################################################
scoreboard = {}
for i in player_order:
    if table[i].asked == table[i].made:
        scoreboard[i] = 10 + 5 * table[i].made
    else:
        scoreboard[i] = -5 * abs(table[i].asked - table[i].made)

player_ranking = sorted(player_order, key=lambda x: -scoreboard[x])
print('THESE ARE THE POINTS')
for i in player_ranking:
    print(i + '\t', 'asked:', table[i].asked, 'made:', table[i].made, 'points:', scoreboard[i])
