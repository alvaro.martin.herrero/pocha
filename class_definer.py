####################################################################################################
# purpose: define all classes used
# date: 20190121
# notes:
####################################################################################################

####################################################################################################
# define classes
####################################################################################################


class Card:

    def __init__(self, suit, suit_number, card_number, label, points):
        self.suit = suit
        self.suit_number = suit_number
        self.card_number = card_number
        self.label = label
        self.points = points


class Player:

    def __init__(self, name, order, cards, cards_thrown_on_table, asked, asked_by_table, made):
        self.name = name
        self.order = order
        self.cards = cards
        self.cards_thrown_on_table = cards_thrown_on_table
        self.asked = asked
        self.asked_by_table = asked_by_table
        self.made = made
