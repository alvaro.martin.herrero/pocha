####################################################################################################
# purpose: each player, in order, make their bets
# date: 20190123
# notes:
####################################################################################################

####################################################################################################
# import modules
####################################################################################################
from set_player_order import player_order
from shuffle import deck
from deal import table

####################################################################################################
# define function for machine and prompt user to make the bet
####################################################################################################
cards_in_hand = len(table['player_me'].cards)
total_bets_asked = 0

print('MAKING BETS')
for i in player_order:
    if i == 'player_me':
        print('your cards are\n', table[i].cards)
        if i == player_order[-1]:
            cannot_bet = cards_in_hand - total_bets_asked
            print('you cannot bet', cannot_bet)
        while True:
            try:
                bet = int(input('how many tricks do you think you will make? (enter a number) \n -->'))
            except ValueError:
                print("that is not even a number!")
                continue
            if bet > 8 or bet < 0:
                print("please enter a positive integer smaller than 8")
            else:
                break
    else:
        bet = sum(1 for x in table[i].cards if deck[x].points > 8)
    table[i].asked = bet
    total_bets_asked += bet
    if i == player_order[-1] and total_bets_asked == cards_in_hand:
        table[player_order[-1]].asked += 1
    print(table[i].name, 'has bet', table[i].asked)

print('---+---+---\n')
