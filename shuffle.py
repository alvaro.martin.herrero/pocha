####################################################################################################
# purpose: set randomly which player starts first
# date: 20190121
# notes:
####################################################################################################

####################################################################################################
# import modules
####################################################################################################
import random
from class_definer import Card

####################################################################################################
# create deck and shuffle
####################################################################################################
card_numbers = {'ace': {'card_number': 1, 'label': 'ace', 'points': 10},
                '3': {'card_number': 3, 'label': '3', 'points': 9},
                'king': {'card_number': 12, 'label': 'king', 'points': 8},
                'horse': {'card_number': 11, 'label': 'horse', 'points': 7},
                'jack': {'card_number': 10, 'label': 'jack', 'points': 6},
                '7': {'card_number': 7, 'label': '7', 'points': 5},
                '6': {'card_number': 6, 'label': '6', 'points': 4},
                '5': {'card_number': 5, 'label': '5', 'points': 3},
                '4': {'card_number': 4, 'label': '4', 'points': 2},
                '2': {'card_number': 2, 'label': '2', 'points': 1}}

card_suits = {'golds': {'label': 'golds', 'suit_number': 1},
              'cups': {'label': 'cups', 'suit_number': 2},
              'swords': {'label': 'swords', 'suit_number': 3},
              'clubs': {'label': 'clubs', 'suit_number': 4}}

deck = {}
for i in card_numbers:
    for j in card_suits:
        deck[i+'_'+j] = Card(suit=card_suits[j]['label'],
                             suit_number=card_suits[j]['suit_number'],
                             card_number=card_numbers[i]['card_number'],
                             label=card_numbers[i]['label']+'_'+card_suits[j]['label'],
                             points=card_numbers[i]['points'])

deck_shuffled = list(deck.keys())
random.shuffle(deck_shuffled)
