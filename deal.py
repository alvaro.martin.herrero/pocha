####################################################################################################
# purpose: deal the deck to already seated players
# date: 20190121
# notes:
####################################################################################################

####################################################################################################
# import modules
####################################################################################################
from set_player_order import player_order
from shuffle import deck, deck_shuffled
from class_definer import Player

####################################################################################################
# deal cards on table
####################################################################################################
table = {}
for i in player_order:
    i_num = player_order.index(i)
    cards_player = deck_shuffled[i_num*8:(i_num+1)*8]
    cards_player = sorted(cards_player, key=lambda x: (deck[x].suit_number, -deck[x].points))
    table[i] = Player(name=i,
                      order=i_num+1,
                      cards=cards_player,
                      cards_thrown_on_table=[],
                      asked=0,
                      asked_by_table=0,
                      made=0)
