# pocha

Fun project in python to allow a person to play a famous Spanish cards game called "pocha" https://es.wikipedia.org/wiki/Pocha_(juego_de_naipes).

This mini-project has been conceived in two phases. As of today 20190128, only the first one "Dumb machine" has been completed.

1. **Dumb machine**:
    * to play this game just clone the repository and type on your command line *python play.py*
    * in this game I just replicate the no-dominant-suit game (*sin pinta* in Spanish)
2. **Clever machine**:
    * This is the interesting part, here I want to teach the machine to play using a neural network and reinforcement learning
    * No progress so far in this respect